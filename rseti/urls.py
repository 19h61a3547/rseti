from django.urls import path

from rseti import views

urlpatterns = [
    path(r'', views.index, name="trust-details.html"),
    path(r'new/', views.newPage),
    path(r'regional-office-details.html', views.next, name="regional-office-details.html"),
    path(r'new1/', views.savePage),
    path(r'2nd-installment-request.html', views.secondInstallmentRequest, name="2nd-instalment-request.html"),
    path(r'2nd-installment-release.html', views.secondInstallmentRelease, name="2nd-instalment-release.html"),
    path(r'building-completion-view', views.buildingCompletionView, name="building-completion.html"),
    path(r'building-construction-status-view', views.buildingConstructionStatusView,
         name="building-construction-status-view.html"),
    path(r'building-construction-status', views.buildingConstructionStatus, name="building-construction-status.html"),
    path(r'building-vist-view',views.buildingVistView,name="building-visit-view.html"),
    path('loadDistricts/', views.loadDistricts)
]

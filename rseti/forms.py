from django import forms

from rseti.models import SecondInstalmentRequest, SecondInstalmentRelease, BuildingCompletion, \
     BuildingVisit,BuildingConstructionStats


class SecondInstalmentRequestForm(forms.ModelForm):
    class Meta:
        model = SecondInstalmentRequest
        fields = '__all__'
        widgets = {
            "secondInstalmentRequestLetterFile": forms.FileInput(attrs={"class": "form-control "}),
            "formGFR12AFile": forms.FileInput(attrs={"class": "form-control "}),
            "formFormat3File": forms.FileInput(attrs={"class": "form-control "}),
            "intrestFirstInstallmentFile": forms.FileInput(attrs={"class": "form-control "}),
            "firstInstallmentAmountFile": forms.FileInput(attrs={"class": "form-control "}),
            "declarationOfBankFile": forms.FileInput(attrs={"class": "form-control "}),
            "photoesOfBuildings": forms.FileInput(attrs={"class": "form-control "}),
            "notionalIntrest": forms.TextInput(attrs={"class": "form-control req"}),
            "approvalOfNIRDPR": forms.TextInput(attrs={"class": "form-control req"})

        }


class SecondInstalmentReleaseForm(forms.ModelForm):
    class Meta:
        model = SecondInstalmentRelease
        fields = "__all__"
        widgets = {
            "dateOfCredit": forms.TextInput(attrs={"class": "form-control req"}),
            "amountRecieved": forms.TextInput(attrs={"class": "form-control req"}),
            "intrestDeducted": forms.TextInput(attrs={"class": "form-control "}),
            "tdsAmount": forms.TextInput(attrs={"class": "form-control req"}),
            "issues": forms.Textarea(attrs={"class": "form-control req"}),

        }


class BuildingCompletionForm(forms.ModelForm):
    class Meta:
        model = BuildingCompletion
        fields = "__all__"
        widgets = {
            "finalGFR12A": forms.FileInput(attrs={"class": "form-control "}),
            "formFormatIII": forms.FileInput(attrs={"class": "form-control "}),
            "buildingCompletionCertificate": forms.FileInput(attrs={"class": "form-control "}),
            "landAndBuildingDetails": forms.FileInput(attrs={"class": "form-control "}),
            "photosOfInteriorAndExteriorOfTheBuildingTaken": forms.Select(attrs={"class": "form-control req"}, choices=(
                ("select", "Select"), ("Yes", "Yes"), ("No", "No"))),
            "photosOfBuilding": forms.FileInput(attrs={"class": "form-control "}),
            "commentsIfAny": forms.Textarea(attrs={"class": "form-control "}),
            "BuiltUpArea": forms.Select(attrs={"class": "form-control req"},
                                        choices=(("select", "Select"), ("Yes", "Greater than 8,000 sqft"),
                                                 ("No", "Less than 8,000 sqft"))),
        }


class BuildingConstructionStatusForm(forms.ModelForm):
    class Meta:
        model = BuildingConstructionStats
        fields = "__all__"
        widgets = {
            "rsetiName": forms.Select(attrs={"class": "form-control req"},
                                      choices=(("select", "Select"), ("Yes", "Greater than 8,000 sqft"),
                                               ("No", "Less than 8,000 sqft"))),
            "functionOfRseti": forms.Select(attrs={"class": "form-control req"},
                                            choices=(
                                                ("select", "Select"), ("MoRD Funded Building", "MoRD Funded Building"),
                                                ("Rent Government Premises", "Rent Government Premises"),
                                                ("Rent Free Government Premises", "Rent Free Government Premises"),
                                                ("Rented Private Premises", "Rented Private Premises"),
                                                ("Rent Free Private Premises", "Rent Free Private Premises"),
                                                ("Own Building", "Own Building"),
                                                ("Not Functioning", "Not Functioning"))),
            "landAllotmentDetails": forms.Select(attrs={"class": "form-control req"},
                                                 choices=(("select", "Select"), ("Not alloted", "Not alloted"),
                                                          ("Alloted", "Alloted"),
                                                          ("Allotted but issues", "Allotted but issues"),
                                                          ("Not required – bank’s own land/building",
                                                           "Not required – bank’s own land/building"),
                                                          ("Not eligible", "Not eligible"))),
            "possessionOfLand": forms.Select(attrs={"class": "form-control req"},
                                             choices=(("select", "Select"), ("Not allotted", "Possession Taken"),
                                                      ("Not allotted", "Possession not taken"))),
            "buildingFundsDetails": forms.Select(attrs={"class": "form-control req"},
                                                 choices=(("select", "Select"), (
                                                     "Not Eligible – partnership with others",
                                                     "Not Eligible – partnership with others"),
                                                          ("Not Eligible – additional RSETI in the district",
                                                           "Not Eligible – additional RSETI in the district"),
                                                          ("Not Eligible – funded by another department/organization",
                                                           "Not Eligible – funded by another department/organization"),
                                                          ("Not Eligible - own building exists",
                                                           "Not Eligible - own building exists"),
                                                          ("Funds needed for renovation of existing own building",
                                                           "Funds needed for renovation of existing own building"),
                                                          ("Funds needed for new construction",
                                                           "Funds needed for new construction"),
                                                          ("Directly funded by MoRD", "Directly funded by MoRD"))),
            "statusOfBuilding": forms.Select(attrs={"class": "form-control req"},
                                             choices=(("select", "Select"),
                                                      ("Yet to start", "Yet to start"),
                                                      ("In progress", "In progress"),
                                                      ("Completed", "Completed"),
                                                      (
                                                      "Construction has been stopped", "Construction has been stopped"),
                                                      ("Not eligible for MoRD Grant-in-Aid",
                                                       "Not eligible for MoRD Grant-in-Aid"),
                                                      ("RSETI closed", "RSETI closed"))),
            "reasonForDelay": forms.Select(attrs={"class": "form-control req"},
                                           choices=(("select", "Select"),
                                                    ("State delay – fees", "State delay – fees"),
                                                    (
                                                    "State delay – approval pending", "State delay – approval pending"),
                                                    ("State delay – land not allotted",
                                                     "State delay – land not allotted"),
                                                    ("State delay – alternate land awaited",
                                                     "State delay – alternate land awaited"),
                                                    ("State delay – litigation/court case",
                                                     "State delay – litigation/court case"),
                                                    ("State delay – encroachment/dispute",
                                                     "State delay – encroachment/dispute"),
                                                    ("State delay – objection from other department(s)",
                                                     "State delay – objection from other department(s)"),
                                                    ("Bank delay – approval pending", ">Bank delay – approval pending"),
                                                    ("Bank delay – funds", "Bank delay – funds"),
                                                    (
                                                    "Bank delay – contractor delay", "Bank delay – contractor delay"))),
            "others": forms.Textarea(attrs={"class": "form-control"}),
            "percentageCompletion": forms.TextInput(attrs={"class": "form-control"}),
            "progress": forms.TextInput(attrs={"class": "form-control"}),
            "photoesOfConstruction": forms.FileInput(attrs={"class": "form-control"}),
            "expectedDate": forms.TextInput(attrs={"class": "form-control"}),
            "completionDate": forms.TextInput(attrs={"class": "form-control"}),
            "rsetiOperationFromNewPremises": forms.Select(attrs={"class": "form-control req"},
                                                          choices=(
                                                              ("select", "Select"), ("Yes", "Yes"),
                                                              ("No", "No"))),
            "shiftingDate": forms.TextInput(attrs={"class": "form-control"}),
            "reason": forms.Textarea(attrs={"class": "form-control"}),
            "resonForStopping": forms.Textarea(attrs={"class": "form-control"}),
            "stepsToResolveIssue": forms.Textarea(attrs={"class": "form-control"}),
        }


class BuildingVisitForm(forms.ModelForm):
    class Meta:
        model = BuildingVisit
        fields = "__all__"
        widgets = {
            "Date": forms.TextInput(attrs={"class": "form-control req"}),
            "RsetiSiteVisitTaken": forms.Select(attrs={"class": "form-control req"},
                                                choices=(("select", "Select"), ("Yes", "Yes"),
                                                         ("No", "No"))),
            "SiteVisitReport": forms.FileInput(attrs={"class": "form-control"}),
            "AttendanceSheet": forms.FileInput(attrs={"class": "form-control"}),
            "NirdprOfficialCertificate": forms.FileInput(attrs={"class": "form-control"}),
            "PhotosOfVisit": forms.FileInput(attrs={"class": "form-control"}),
            "OtherDocuments": forms.FileInput(attrs={"class": "form-control"}), }

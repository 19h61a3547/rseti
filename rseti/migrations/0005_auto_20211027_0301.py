# Generated by Django 3.2.8 on 2021-10-26 21:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('rseti', '0004_buildingcompletion'),
    ]

    operations = [
        migrations.CreateModel(
            name='BuildingConstructionStatus',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rsetiName', models.CharField(max_length=200)),
                ('functionOfRseti', models.CharField(max_length=200)),
                ('landAllotmentDetails', models.CharField(max_length=200)),
                ('possessionOfLand', models.CharField(max_length=200)),
                ('buildingFundsDetails', models.CharField(max_length=200)),
                ('statusOfBuilding', models.CharField(max_length=200)),
                ('reasonForDelay', models.CharField(max_length=200)),
                ('others', models.CharField(max_length=1000)),
                ('percentageCompletion', models.IntegerField()),
                ('progress', models.CharField(max_length=500)),
                ('photoesOfConstruction', models.FileField(upload_to='')),
                ('expectedDate', models.CharField(max_length=200)),
                ('completionDate', models.CharField(max_length=200)),
                ('rsetiOperationFromNewPremises', models.CharField(max_length=200)),
                ('shiftingDate', models.CharField(max_length=200)),
                ('reason', models.CharField(max_length=1000)),
                ('resonForStopping', models.CharField(max_length=1000)),
                ('stepsToResolveIssue', models.CharField(max_length=1000)),
            ],
        ),
        migrations.CreateModel(
            name='District',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.AlterField(
            model_name='regionalofficedetails',
            name='district',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rseti.district'),
        ),
        migrations.AlterField(
            model_name='regionalofficedetails',
            name='state',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rseti.state'),
        ),
    ]
